import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {InboxData} from '../share/interface/inbox-data.interface';
import 'rxjs/add/operator/finally';

@Component({
  selector: 'app-inbox-container',
  templateUrl: './inbox-container.component.html',
  styleUrls: ['./inbox-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InboxContainerComponent {

  @Input()
  enableLazyLoad: boolean = true;
  @Input()
  inboxDataList: InboxData[] = [] as InboxData[];
  @Input()
  isLastPage?: boolean;
  @Input()
  isError?: boolean = false;
  @Input()
  errorMsg?: string = '';
  @Output()
  onLoadNextPage?: EventEmitter<void> = new EventEmitter<void>();

  @Input()
  isLoading?: boolean = false;


  @HostListener('scroll', ['$event'])
  onScroll(event: any): void {
    if (this.enableLazyLoad && !this.isLoading && !this.isLastPage && event.target.offsetHeight + event.target.scrollTop === event.target.scrollHeight) {
      this.onLoadNextPage.emit();
    }
  }

  onItemClick(inboxData: InboxData) {
    const itemIndex = this.inboxDataList.findIndex(item => item === inboxData);
    for (let i = 0; i < this.inboxDataList.length; i++) {
      if (itemIndex !== i) {
        this.inboxDataList[i] = {...this.inboxDataList[i], isSelect: false};
      } else {
        this.inboxDataList[i] = {...this.inboxDataList[i], isSelect: !this.inboxDataList[i].isSelect};
      }
    }
  }


  trackByIndex(index: number): any {
    return index;
  }

}
