import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {ShareModule} from './share/share.module';
import {FormsModule} from '@angular/forms';
import {InboxItemComponent} from './inbox-container/inbox-item/inbox-item.component';
import {InboxContainerComponent} from './inbox-container/inbox-container.component';
import {InboxData} from './share/interface/inbox-data.interface';
import SpyObj = jasmine.SpyObj;
import {InboxService} from './share/service/inbox.service';
import 'rxjs/add/operator/finally';
import {InboxDataPagination} from './share/interface/inbox-data-pagination.interface';
import {Observable} from 'rxjs/Rx';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let inboxService: SpyObj<InboxService>;

  beforeEach(async(() => {
    inboxService = jasmine.createSpyObj('InboxService', ['fetchInboxList']);
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        InboxContainerComponent,
        InboxItemComponent
      ],
      imports: [
        BrowserModule,
        ShareModule,
        FormsModule
      ],
      providers: [
        {provide: InboxService, useValue: inboxService}
      ]
    }).compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  describe('#fetchData', () => {

    it('should set dataSources and isLoading to be false when there is data', () => {

      //GIVEN
      const mockData = {
        last: false,
        totalElements: 1,
        totalPages: 1,
        size: 1,
        number: 1,
        sort: 'test',
        first: false,
        numberOfElements: 1,
        content: [
          {
            icon: {
              iconShortText: 'NT',
              iconColorCode: '#F08080'
            },
            from: {
              name: 'Now TV',
              email: 'nowtv@test.com'
            },
            subject: 'Grab another Pass, you need to be watching this...',
            body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting',
            createdTime: '11:15',
            isRead: false,
            isSelect: false
          },
          {
            icon: {
              iconShortText: 'NT2',
              iconColorCode: '#F08080'
            },
            from: {
              name: 'Now TV2',
              email: 'nowtv@test.com2'
            },
            subject: 'Grab another Pass, you need to be watching this...2',
            body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting2',
            createdTime: '11:152',
            isRead: false,
            isSelect: false
          }
        ] as InboxData[]
      } as InboxDataPagination;
      inboxService.fetchInboxList.and.returnValues(Observable.of(mockData));

      //WHEN
      component.fetchData(5, 0);

      //THEN
      expect(inboxService.fetchInboxList).toHaveBeenCalledWith(5, 0);
      expect(component.inboxDataList.length).toBe(mockData.content.length);
      expect(component.isLoading).toBeFalsy();

    });
  });


  describe('#onLoadNextPage', () => {

    it('should increase 1 when call onLoadNextPage function', () => {

      //GIVEN
      component.pageNumber = 0;
      component.pageSize = 5;

      spyOn(component, 'fetchData');

      //WHEN
      component.onLoadNextPage();

      //THEN
      expect(component.fetchData).toHaveBeenCalledWith(component.pageSize, 1);

    });
  });


});
