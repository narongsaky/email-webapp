import {Component, OnInit} from '@angular/core';
import {InboxService} from './share/service/inbox.service';
import {InboxDataPagination} from './share/interface/inbox-data-pagination.interface';
import {InboxData} from './share/interface/inbox-data.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  pageSize: number = 5;
  pageNumber: number = 0;
  isLastPage: boolean = false;
  isLoading: boolean = true;
  inboxDataList: InboxData[] = [] as InboxData[];

  inboxDataListNoData: InboxData[] = [] as InboxData[];

  constructor(private inboxService: InboxService) {

  }

  ngOnInit() {
    this.fetchData(this.pageSize, this.pageNumber);
  }

  fetchData(pageSize: number, pageNumber: number): void {
    this.isLoading = true;
    this.inboxService.fetchInboxList(pageSize, pageNumber)
      .finally(() => {
        this.isLoading = false;
      })
      .subscribe((response: InboxDataPagination) => {
        this.inboxDataList = this.inboxDataList.concat(response.content);
        this.isLastPage = response.last;
      });
  }

  onLoadNextPage() {
    this.pageNumber ++;
    this.fetchData(this.pageSize, this.pageNumber);
  }

}
