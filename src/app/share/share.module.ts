import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {InboxService} from './service/inbox.service';


@NgModule({
  declarations: [],
  imports: [HttpClientModule],
  providers: [InboxService]
})
export class ShareModule {

}
