import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/delay';
import {InboxDataPagination} from '../interface/inbox-data-pagination.interface';


@Injectable()
export class InboxService {

  constructor(private httpClient: HttpClient) {

  }

  fetchInboxList(pageSize: number, pageNumber: number): Observable<InboxDataPagination> {
    return this.httpClient.get<InboxDataPagination>('assets/inbox-list-page-' + pageNumber + '.json').delay(2000);
  }


}
