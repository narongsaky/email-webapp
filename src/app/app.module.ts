import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {ShareModule} from './share/share.module';
import { InboxContainerComponent } from './inbox-container/inbox-container.component';
import { InboxItemComponent } from './inbox-container/inbox-item/inbox-item.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    InboxContainerComponent,
    InboxItemComponent
  ],
  imports: [
    BrowserModule,
    ShareModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
