import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxContainerComponent } from './inbox-container.component';
import {BrowserModule, By} from '@angular/platform-browser';
import {InboxItemComponent} from './inbox-item/inbox-item.component';
import {FormsModule} from '@angular/forms';
import {ChangeDetectionStrategy} from '@angular/core';
import {InboxData} from '../share/interface/inbox-data.interface';

describe('InboxContainerComponent', () => {
  let component: InboxContainerComponent;
  let fixture: ComponentFixture<InboxContainerComponent>;
  let element: any;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [ InboxContainerComponent, InboxItemComponent ],
      imports: [
        FormsModule,
        BrowserModule
      ]
    }).overrideComponent(InboxContainerComponent, {
      set: {changeDetection: ChangeDetectionStrategy.Default}
    }).compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxContainerComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render correctly when there is data.', () => {
    //GIVEN
    component.enableLazyLoad = false;
    component.inboxDataList = [
      {
        icon: {
          iconShortText: 'NT',
          iconColorCode: '#F08080'
        },
        from: {
          name: 'Now TV',
          email: 'nowtv@test.com'
        },
        subject: 'Grab another Pass, you need to be watching this...',
        body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting',
        createdTime: '11:15',
        isRead: false,
        isSelect: false
      },
      {
        icon: {
          iconShortText: 'NT2',
          iconColorCode: '#F08080'
        },
        from: {
          name: 'Now TV2',
          email: 'nowtv@test.com2'
        },
        subject: 'Grab another Pass, you need to be watching this...2',
        body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting2',
        createdTime: '11:152',
        isRead: false,
        isSelect: false
      }
    ] as InboxData[];

    fixture.detectChanges();

    //WHEN
    const appInboxItemElems = element.queryAll(By.css('#appInboxItem'));
    const errorDivElems = element.queryAll(By.css('#errorDiv'));
    const noDataDivElems = element.queryAll(By.css('#noDataDiv'));

    //THEN
    expect(appInboxItemElems.length).toBe(2);
    expect(errorDivElems.length).toBe(0);
    expect(noDataDivElems.length).toBe(0);

  });

  it('should render empty data when there is no data.', () => {
    //GIVEN
    component.enableLazyLoad = false;
    component.inboxDataList = [] as InboxData[];

    fixture.detectChanges();

    //WHEN
    const appInboxItemElems = element.queryAll(By.css('#appInboxItem'));
    const errorDivElems = element.queryAll(By.css('#errorDiv'));
    const noDataDivElems = element.queryAll(By.css('#noDataDiv'));

    //THEN
    expect(appInboxItemElems.length).toBe(0);
    expect(errorDivElems.length).toBe(0);
    expect(noDataDivElems.length).toBe(1);

  });


  it('should render error msg when error.', () => {
    //GIVEN
    component.enableLazyLoad = false;
    component.isError = true;
    component.errorMsg = 'something went wrong';
    component.inboxDataList = [] as InboxData[];

    fixture.detectChanges();

    //WHEN
    const appInboxItemElems = element.queryAll(By.css('#appInboxItem'));
    const errorDivElems = element.queryAll(By.css('#errorDiv'));
    const noDataDivElems = element.queryAll(By.css('#noDataDiv'));

    //THEN
    expect(appInboxItemElems.length).toBe(0);
    expect(errorDivElems[0].nativeElement.textContent).toContain(component.errorMsg);
    expect(noDataDivElems.length).toBe(0);

  });


  it('should update isSelect when call onItemClick function.', () => {
    //GIVEN
    component.enableLazyLoad = false;
    component.isError = true;
    component.errorMsg = 'something went wrong';
    component.inboxDataList = [
      {
        icon: {
          iconShortText: 'NT',
          iconColorCode: '#F08080'
        },
        from: {
          name: 'Now TV',
          email: 'nowtv@test.com'
        },
        subject: 'Grab another Pass, you need to be watching this...',
        body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting',
        createdTime: '11:15',
        isRead: false,
        isSelect: false
      },
      {
        icon: {
          iconShortText: 'NT2',
          iconColorCode: '#F08080'
        },
        from: {
          name: 'Now TV2',
          email: 'nowtv@test.com2'
        },
        subject: 'Grab another Pass, you need to be watching this...2',
        body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting2',
        createdTime: '11:152',
        isRead: false,
        isSelect: false
      }
    ] as InboxData[];
    fixture.detectChanges();

    //WHEN
    component.onItemClick(component.inboxDataList[0]);

    //THEN
    expect(component.inboxDataList[0].isSelect).toBeTruthy();
    expect(component.inboxDataList[1].isSelect).toBeFalsy();
  });


});
