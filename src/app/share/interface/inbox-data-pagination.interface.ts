import {InboxData} from './inbox-data.interface';

export interface InboxDataPagination {
  last: boolean;
  totalElements: number;
  totalPages: number;
  size: number;
  number: number;
  sort: string;
  first: boolean;
  numberOfElements: number;
  content: InboxData[];
}
