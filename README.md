## Installation

Install all of the dependencies before doing anything.

```bash
npm install
```


## Run

Start app by use default environment config or run by production environment

```bash
ng serve
ng serve --prod
```

Once run one of this command success you can open **http://localhost:4200/**

## Test

Run unit test command.
```bash
ng test
```

## Performance 

This is a way for improved performance in this application.

- **Lazy load** data mechanism to get data from server by pagination.
- Use **ChangeDetectionStrategy.OnPush**.
- Use **trackBy** in *ngFor to track item index.

---


## Dependency && Methodology 

- **roboto-fontface** to use roboto font.
- **bootstrap** popular css library there are many utility class and responsive.
- **font-awesome** to get pretty icon.
- **BEM Css** this is a methodology that use in this application.
