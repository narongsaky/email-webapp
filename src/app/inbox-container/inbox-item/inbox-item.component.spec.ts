import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {InboxItemComponent} from './inbox-item.component';
import {FormsModule} from '@angular/forms';
import {BrowserModule, By} from '@angular/platform-browser';
import {InboxData} from '../../share/interface/inbox-data.interface';
import {ChangeDetectionStrategy} from '@angular/core';

describe('InboxItemComponent', () => {
  let component: InboxItemComponent;
  let fixture: ComponentFixture<InboxItemComponent>;
  let element: any;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [InboxItemComponent],
      imports: [
        FormsModule,
        BrowserModule
      ]
    }).overrideComponent(InboxItemComponent, {
      set: {changeDetection: ChangeDetectionStrategy.Default}
    }).compileComponents();


  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxItemComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render correctly when there is data.', () => {
    component.inboxData = {
      icon: {
        iconShortText: 'NT',
        iconColorCode: '#F08080'
      },
      from: {
        name: 'Now TV',
        email: 'nowtv@test.com'
      },
      subject: 'Grab another Pass, you need to be watching this...',
      body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting',
      createdTime: '11:15',
      isRead: false,
      isSelect: false
    } as InboxData;

    fixture.detectChanges();

    const mainElems = element.queryAll(By.css('inbox-item'));
    const inboxItemNameElems = element.queryAll(By.css('#inboxItemName'));
    const inboxItemSubjectElems = element.queryAll(By.css('#inboxItemSubject'));
    const inboxItemCreatedTimeElems = element.queryAll(By.css('#inboxItemCreatedTime'));
    const inboxItemBodyElems = element.queryAll(By.css('#inboxItemBody'));

    expect(mainElems).toBeDefined();
    expect(inboxItemNameElems[0].nativeElement.textContent).toContain(component.inboxData.from.name);
    expect(inboxItemSubjectElems[0].nativeElement.textContent).toContain(component.inboxData.subject);
    expect(inboxItemCreatedTimeElems[0].nativeElement.textContent).toContain(component.inboxData.createdTime);
    expect(inboxItemBodyElems[0].nativeElement.textContent).toContain(component.inboxData.body);

  });


  it('should emit inboxData when call onItemClickAction function.', () => {

    //GIVEN
    component.inboxData = {
      icon: {
        iconShortText: 'NT',
        iconColorCode: '#F08080'
      },
      from: {
        name: 'Now TV',
        email: 'nowtv@test.com'
      },
      subject: 'Grab another Pass, you need to be watching this...',
      body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting',
      createdTime: '11:15',
      isRead: false,
      isSelect: false
    } as InboxData;
    fixture.detectChanges();

    spyOn(component.onItemClick, 'emit');

    //WHEN
    component.onItemClickAction();

    //THEN
    expect(component.onItemClick.emit).toHaveBeenCalledWith(component.inboxData);

  });

});
