export interface InboxData {
  icon: {
    iconShortText: string;
    iconColorCode: string;
  };
  from: {
    name: string;
    email: string;
  };
  subject: string;
  body: string;
  createdTime: string;
  isRead: boolean;
  isSelect: boolean;
}
