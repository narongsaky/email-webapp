import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {InboxData} from '../../share/interface/inbox-data.interface';


@Component({
  selector: 'app-inbox-item',
  templateUrl: './inbox-item.component.html',
  styleUrls: ['./inbox-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InboxItemComponent {

  @Input()
  inboxData: InboxData = {} as InboxData;

  @Output()
  onItemClick = new EventEmitter<any>();

  onItemClickAction(): void {
    this.onItemClick.emit(this.inboxData);
  }

}
